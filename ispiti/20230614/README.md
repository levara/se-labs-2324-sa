# Pismeni ispit 23.02.2023.

## NAPOMENE

### Pazite na `git config`, postavite svoje ime i email prije commitanja

- Svaki zadatak riješiti i spremiti u vlastiti commit, nakon
rješavanja ispita pushati sve na svoj repozitorij

- Ne miješati fileove različitih zadataka u isti commit. U commitu za pojedini
  zadatak trebaju biti samo oni fileovi koji direktno utječu na taj zadatak.

- U slučaju greške pri commitu, napraviti novi commit s ispravkom i nazvati ga
  "Fix za N. zadatak", gdje je N broj zadatka za koji se radi ispravak

- U commitovima ignorirati virtual environment i pycache fileove.


## Zadaci

### 1. Pokrenuti projekt (5 bodova)

- Preuzeti promjene sa repozitorija i spojiti ih u svoj repozitorij
- Pokrenuti igru
- Promjeniti boju svih tekstova u igri u plavu

### 2. Healthpack rotation (5 bodova)

- HealthPack se rotira na mjestu, svaki frame za jedan stupanj
- Pri stvaranju HealthPacka na ekranu, slučajno odabere smjer rotacije

### 3. Kolizija turreta i asteroida (5 bodova)

- Kada dođe do kolizije između asteroida i turreta, oba trebaju biti uništena

### 4. Teleport (5 bodova)

- Izmjeniti kod za teleportiranje igrača tako da više nema cooldowna, igrač se
  može teleportirati odmah bez čekanja, ali ima ukupno 3 teleportiranja po
  levelu.
- Umjesto cooldown timera staviti tekst koliko je igraču preostalo
  teleportiranja.

### 5. Brzina asteroida (10 bodova)

- Brzina asteroida je jednaka levelu na kojem smo trenutno. Npr. Level 1, brzina je 1, za level 5 brzina je 5, itd.

### 6. Clear asteroids (20 bodova)

- Na pritisak tipke SPACE svaki asteroid se postavi u jedan od 4 kuta ekrana, ovisno o tome koji kut je tom asteroidu najbliži. Ova mogućnost smije se iskoristiti jednom po levelu. Asteroid zadržava brzinu i smjer kretanja, samo mu se promjeni lokacija na najbliži kut.

Recimo, ako je x,y asteroida 500,500, budući da je rezolucija 1600,900, najbliži kut je lijevo dolje. Tako će novi x,y za asteroid biti (1500,800) (Ostaviti 100 piksela margine od ruba).






