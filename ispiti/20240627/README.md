<!--- markdown_here --->
# Pismeni ispit 27.06.2023.

## NAPOMENE

### Pazite na `git config`, postavite svoje ime i email prije commitanja

- Svaki zadatak riješiti i spremiti u vlastiti commit, nakon
rješavanja ispita pushati sve na svoj repozitorij

- Ne miješati fileove različitih zadataka u isti commit. U commitu za pojedini
  zadatak trebaju biti samo oni fileovi koji direktno utječu na taj zadatak.

- U slučaju greške pri commitu, napraviti novi commit s ispravkom i nazvati ga
  "Fix za N. zadatak", gdje je N broj zadatka za koji se radi ispravak

- U commitovima ignorirati virtual environment i pycache fileove.


## Zadaci

### 1. Pokrenuti projekt (5 bodova)

- Preuzeti promjene sa repozitorija i spojiti ih u svoj repozitorij
- Pokrenuti igru
- Promjeniti boju svih tekstova u igri u bijelu

### 2. Štit za playera (5 bodova)

- Na slici ispod nalazi se slika štita, u folderu se zove `shield.png`. Omogućiti da se štit prikazuje za cijelo vrijeme iznad playera ako je pritisnuta tipka SPACE.  

<img src="./shield.png" style="width: 200px; height: auto;"/>

- Ovako treba izgledati kada je štit aktivan.

<img src="./screenshot.png" style="width: 600px; height: auto;"/>

### 3. Štit za asteroide (5 bodova)

- Na slici ispod nalazi se slika štita, u folderu se zove `shield.png`. Omogućiti da se štit prikazuje za cijelo vrijeme iznad svakog asteroida.  


### 3. SPACE gasi štitove (10 bodova)

- Dok je pritisnut SPACE niti player niti asteroidi nemaju štit


### 4. Štit štiti dok je aktivan (10 bodova)

- štit je neaktivan na pritisak SPACE i tada je moguće pogoditi asteroid. Isto tako i turreti mogu pogoditi playera ako je štit neaktivan.
- Štit kada je aktivan uništava bullete. Ovo vrijedi i za turretove i za playerove bullete. U prijevodu, player može pucati samo ako je playerov štit neaktivan, jer inače i playerov štit uništava playerove bullete.
- Štit ne oštećuje niti jedan drugi entitet osim bulleta. Kontakt asteroida ili turreta sa štitom ne radi štetu.

### 5. Turret može oštetiti i asteroide (15 bodova)

- Turret kada puca, njegovi bulleti mogu oštetiti i asteroide ako ih pogode.
- Pogodak asteroida odmah ga uništava.
- Pogodak asteroida moguć je samo ako je štit na asteroidu neaktivan.

