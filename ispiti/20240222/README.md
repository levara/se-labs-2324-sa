<!--- markdown_here --->
# Pismeni ispit 23.02.2023.

## NAPOMENE

### Pazite na `git config`, postavite svoje ime i email prije commitanja

- Svaki zadatak riješiti i spremiti u vlastiti commit, nakon
rješavanja ispita pushati sve na svoj repozitorij

- Ne miješati fileove različitih zadataka u isti commit. U commitu za pojedini
  zadatak trebaju biti samo oni fileovi koji direktno utječu na taj zadatak.

- U slučaju greške pri commitu, napraviti novi commit s ispravkom i nazvati ga
  "Fix za N. zadatak", gdje je N broj zadatka za koji se radi ispravak

- U commitovima ignorirati virtual environment i pycache fileove.


## Zadaci

### 1. Pokrenuti projekt (5 bodova)

- Preuzeti promjene sa repozitorija i spojiti ih u svoj repozitorij
- Pokrenuti igru
- Promjeniti boju svih tekstova u igri u bijelu

### 2. Štit (15 bodova)

- Na slici ispod nalazi se slika štita, u folderu se zove `shield.png`. Omogućiti da se štit prikazuje za cijelo vrijeme iznad svakog asteroida.  

<img src="./shield.png" style="width: 200px; height: auto;"/>

- Primjer s prošlog roka kako štit izgleda kada je iznad playera. Postavite veličinu štita ispravno, treba biti okrugao, i obuhvaćati cijeli asteroid.

<img src="./screenshot.png" style="width: 600px; height: auto;"/>

### 3. SPACE gasi štitove (5 bodova)

- Dok je pritisnut SPACE asteroidi nemaju štit

### 4. Štit štiti dok je aktivan (15 bodova)

- štit je neaktivan na pritisak SPACE i tada je moguće pogoditi asteroid. 
- Štit kada je aktivan uništava playerove bullete.
- Štit ne oštećuje niti jedan drugi entitet osim playerovih bulleta

### 5. Punjenje štita (10 bodova)

- Onemogućavanje štita troši energiju. 
- Razina onemogućavanja štita ispisuje se na ekranu kao "Shield Disruption: 100" i kreće sa 100 energije. 
- Svaki protekli frame igre energija štita se puni za 0.2
- Svaki protekli frame kada je štit aktivan energija štita se umanjuje za 1

