# Importing the pygame library
import pygame
import random

class Player:
    def __init__(self, screen, image_path):
        self.screen = screen
        self.x = random.randint(0, 500)
        self.y = random.randint(0, 500)
        self.dir_x = random.choice( [-1, 0, 1] )
        self.dir_y = random.choice( [-1, 0, 1] )
        self.w = 140
        self.h = 100
        self.vel = 1
        self.image = pygame.image.load(image_path)

    def update_position(self):
        if self.x >= WIDTH - 1 - self.w or self.x <= 0:
            self.dir_x = self.dir_x * -1
        if self.y >= HEIGHT - 1 - self.h or self.y <= 0:
            self.dir_y = self.dir_y * -1

        self.x += self.dir_x * self.vel
        self.y += self.dir_y * self.vel

    def draw(self):
        self.scaled = pygame.transform.scale(self.image, [self.w, self.h])
        self.drawn = self.screen.blit(self.scaled, [self.x, self.y])

    def clicked(self):
        self.x = random.randint(1, WIDTH - self.w)
        self.y = random.randint(1, HEIGHT - self.h)
        self.vel += 1
        self.dir_x = random.choice([-1, 0, 1])
        self.dir_y = random.choice([-1, 0, 1])
        # Ensuring that the image does not stand still
        if self.dir_y == 0 and self.dir_x == 0:
            self.dir_x = random.choice([-1, 1])
            self.dir_y = random.choice([-1, 1])
        self.w += 14
        self.h += 10

    def increase_velocity(self):
        self.vel += 1

    def decrease_velocity(self):
        self.vel -= 1

# Initialization
pygame.init()
pygame.font.init()

# Defining constants for window size
WIDTH = 1024
HEIGHT = 600
# Tuple for window size
SIZE = (WIDTH, HEIGHT)

# Colors
WHITE = [255, 255, 255]
BLACK = [0, 0, 0]
RED = [255, 0, 0]
GREEN = [0, 255, 0]
BLUE = [0, 0, 255]
MAGENTA = [255, 0, 255]
ORANGE = [255, 88, 0]

# Creating a new game screen
screen = pygame.display.set_mode(SIZE)

# Setting the window title
pygame.display.set_caption("Moja prva igra")

# Font setup
my_font = pygame.font.SysFont("Arial", 40)
score_text = my_font.render("Score: ", True, RED)
lives_text = my_font.render("Lives: ", True, GREEN)

# Clock for managing frames
clock = pygame.time.Clock()

num_players = 8
images = ["slika.png", "slika2.jpg"]
players = []
for _ in range(num_players):
    image_path = random.choice(images)
    player = Player(screen, image_path)
    players.append(player)


# Game variables
score = 0
lives = 5
state = "welcome"  # states: welcome, game, gameover
dt = 0
welcome_screen_time = 300

# Background color
bg = ORANGE

# Game loop
done = False
while not done:
    # Event loop
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            done = True

        if event.type == pygame.KEYDOWN:
            if state == "gameover":
                if event.key == pygame.K_RETURN:
                    state = "welcome"
                    score = 0
                    lives = 5
                    welcome_screen_time = 3000
                    vel = 1
                    dt = 0

            if event.key == pygame.K_SPACE:
                if bg == ORANGE:
                    bg = MAGENTA
                elif bg == MAGENTA:
                    bg = BLUE
                else:
                    bg = ORANGE

            if event.key == pygame.K_w:
                for player in players:
                    player.increase_velocity()
            if event.key == pygame.K_s:
                for player in players:
                    player.decrease_velocity()

        if event.type == pygame.MOUSEBUTTONDOWN:
            click = event.pos
            # Checking if the image has been clicked
            miss = True
            for player in players:
                if player.drawn.collidepoint(click):
                    score += 1
                    player.clicked()
                    miss = False
            if miss:
                lives -= 1

                        

    # World state calculations
    if state == "welcome":
        welcome_screen_time -= dt
        if welcome_screen_time <= 0:
            state = "game"
    elif state == "game":
        if lives <= 0:
            state = "gameover"

        for player in players:
            player.update_position()
        

    # Rendering
    if state == "welcome":
        screen.fill(GREEN)
        welcome_text = my_font.render("Welcome!", True, RED)
        welcome_time_text = my_font.render(
            f"{welcome_screen_time / 1000:.2f}", True, RED
        )
        screen.blit(welcome_text, [100, 100])
        screen.blit(welcome_time_text, [200, 200])
    elif state == "game":
        screen.fill(bg)
        for player in players:
            player.draw()
        
        score_text = my_font.render(f"Score: {score}", True, WHITE)
        lives_text = my_font.render(f"Lives: {lives}", True, WHITE)
        screen.blit(score_text, [20, 20])
        screen.blit(lives_text, [20, 70])
    elif state == "gameover":
        screen.fill(BLUE)
        gameover_text = my_font.render("Gameover", True, WHITE)
        score_text = my_font.render(f"Score: {score}", True, WHITE)
        screen.blit(gameover_text, [100, 100])
        screen.blit(score_text, [200, 200])

    pygame.display.flip()

    # Frame rate control to achieve 60fps
    dt = clock.tick(60)

# Quitting the game
pygame.quit()
