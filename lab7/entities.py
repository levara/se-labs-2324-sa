import pygame
import random
import math

class Geometry(object):

    @classmethod
    def get_direction_from_angle(cls, angle, length):
        """ Return x,y direction vector from angle and length"""
        rad_angle = math.radians(angle)
        x = length * math.cos(rad_angle)
        y = length * math.sin(rad_angle)
        return x,y

    @classmethod
    def get_angle_from_line(cls, x1, y1, x2, y2):
        """ Return angle in degrees from two points: 
            
            Params: 
            x1, y1 coordinates for point 1
            x2, y2 coordinates for point 2
        """
        dirx = x2 - x1
        diry = y2 - y1
        rad_angle = math.atan2(diry, dirx)
        deg_angle = rad_angle * 180/math.pi
        return deg_angle % 360


class Player(object):
    """Handles all the logic for the player object"""

    def __init__(self, screen, image):
        self.screen = screen
        self.sw, self.sh = screen.get_size()
        self.w, self.h = (130, 70)
        self.x, self.y = self.sw/2, self.sh/2
        self.dx, self.dy = 1,1
        self.dir = [0,0]
        self.vel = 1
        self.angle = 0
        self.d_angle = 2
        self.image = image
        self.drawn = None

    def update(self):
        self.move()
        #self.rect = pygame.Rect(self.x - self.w/2, self.y-self.h/2, self.w, self.h)

    def draw(self):
        ship_img = pygame.transform.scale(self.image, (self.w, self.h))
        ship_img = pygame.transform.rotate(ship_img, -self.angle)
        self.rect = ship_img.get_rect(center = (self.x, self.y))
        self.drawn = self.screen.blit(ship_img, self.rect)

    def move(self):
        keys = pygame.key.get_pressed()

        if keys[pygame.K_a]:
            self.angle += self.d_angle
        elif keys[pygame.K_d]:
            self.angle -= self.d_angle
        self.angle = self.angle % 360

        self.dx, self.dy = Geometry.get_direction_from_angle(self.angle, 1)

        pygame.draw.circle(self.screen, (255,0,0), (int(self.x + self.dx*100), int(self.y + self.dy*100)), 3)

        if keys[pygame.K_w]:
            self.x += self.dx
            self.y += self.dy
        elif keys[pygame.K_s]:
            self.x -= self.dx
            self.y -= self.dy



class Asteroid(object):
    """Handles all the logic for the player object"""
    PLAYER_DEAD_ZONE = 300

    def __init__(self, screen):
        self.screen = screen
        self.sw, self.sh = screen.get_size()
        self.radius = random.randint(10,40)
        self.init_random_position()
        self.dx, self.dy = random.choice([-1,1]), random.choice([-1,1])

    def update(self):
        #self.rect = pygame.Rect(self.x - self.w/2, self.y-self.h/2, self.w, self.h)
        self.move()

    def draw(self):
        pygame.draw.circle(self.screen, (33,33,33), (self.x, self.y), self.radius)

    def init_random_position(self):
        """
        Create x,y coordinates that are at least PLAYER_DEAD_ZONE pixels away
        from the player (center of the screen)."""
        left = random.randint(self.radius, self.sw/2 - self.PLAYER_DEAD_ZONE)
        right = random.randint(self.sw/2+self.PLAYER_DEAD_ZONE, self.sw - self.radius)
        top = random.randint(self.radius, self.sh/2 - self.PLAYER_DEAD_ZONE)
        bottom = random.randint(self.sh/2+self.PLAYER_DEAD_ZONE, self.sh - self.radius)
        self.x = random.choice( [left, right] )
        self.y = random.choice( [top, bottom] )

    def move(self):
        if self.x >= self.sw - self.radius or self.x <= self.radius:
            self.dx *= -1
        if self.y >= self.sh - self.radius or self.y <= self.radius:
            self.dy *= -1
        self.x = self.x + self.dx
        self.y = self.y + self.dy


