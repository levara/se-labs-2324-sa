# ukljucivanje biblioteke pygame
import pygame

pygame.init()
# definiranje konstanti za velicinu prozora
WIDTH = 1024
HEIGHT = 600
# tuple velicine prozora
size = (WIDTH, HEIGHT)
WHITE = [ 255, 255, 255 ]
BLACK = [ 0, 0, 0 ]
RED = [ 255, 0, 0 ]
GREEN = [0, 255, 0 ]
BLUE = [0, 0, 255 ]
MAGENTA = [ 255, 0, 255 ]
ORANGE = [ 255, 88, 0 ]

#definiranje novog ekrana za igru
screen = pygame.display.set_mode(size)
#definiranje naziva prozora
pygame.display.set_caption("Nasa kul igra")

clock = pygame.time.Clock()
slikaw = 140
slikah = 100
slika = pygame.image.load("lab3/slika.png")
slika = pygame.transform.scale(slika, [ slikaw,slikah ])
bg = ORANGE

done = False
while not done:
    #event petlja
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            done = True
        if event.type == pygame.KEYDOWN:
            if event.key == pygame.K_SPACE:
                if bg == ORANGE:
                    bg = MAGENTA
                elif bg == MAGENTA:
                    bg = BLUE
                elif bg == BLUE:
                    bg = ORANGE

    screen.fill( bg )
    screen.blit(slika, [WIDTH/2-slikaw/2, HEIGHT/2-slikah/2])

    pygame.display.flip()

    #ukoliko je potrebno ceka do iscrtavanja 
    #iduceg framea kako bi imao 60fpsa
    clock.tick(60)

pygame.quit()

