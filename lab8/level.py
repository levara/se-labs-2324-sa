from helpers import *
from entities import Player, Asteroid

class Level(object):
    ASTEROID_COUNT = 10

    SHIP = pygame.image.load("ship_on.png")
    ASTEROID = pygame.image.load("asteroid.png")
    BULLET = pygame.image.load("laser.png")

    def __init__(self, screen):
        self.screen = screen

        self.shipimage = self.SHIP
        self.asteroidimage = self.ASTEROID
        self.bulletimage = self.BULLET

        self.player = Player(self)
        self.asteroids = [ Asteroid(self) for i in range(self.ASTEROID_COUNT) ]
        self.bullets = []
        
    def update(self):
        for asteroid in self.asteroids:       
            asteroid.update()
        for bullet in self.bullets:
            bullet.update()
        self.player.update()  

    def draw(self):
        for asteroid in self.asteroids:
            asteroid.draw()
        for bullet in self.bullets:
            bullet.draw()
        self.player.draw()

